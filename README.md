0) PLOT
  plot( funct )
  plot( funct; xini; xlast )

1) INTERSECCION
  intersection( fun1, fun2 )
  intersection( fun1, fun2 , xmin, xmax)

2) INTEGRALES
  integrate( function; xmin; xmax)
  integrate( function; xmin; xmax; method )

3) AREA
  area( funct1, funct2)
  area( funct1, funct2, xmin, xmax)

4) RAICES
  allroots( funct )
  root( func; a; b; biseccion)
  root( func; a; b; falsaposicion)
  root( func; funcDerivada; a; newton)
  root( func; a; secante)

5) INTERPOLACION
   interpolation(filename)

6) EDO's
  //edo( funct; xini; yini; xlast)
  //edo( funct; xini; yini; xlast; method)
    edo( funct; xini; yini; xlast; n)
  edo( funct; xini; yini; xlast; n; method)

7) EXTRAPOLACION
   extrapolation( filename; method)
   extrapolation( filename )

8) EVALUAR
  evaluate( double)
  evaluate( funct ;double)

probar edos
(x*x+3*y)/x
-x*y/(x*x+9)    0, 1, 10
(-3*exp(x)*tan(y))/((2-exp(x))*power(sec(y),2))
-3*exp(x)*tan(y)/(2-exp(x))*power(1/cos(y),2)
(4-12*y)/3
(1+x)/(x*x*y*y)   1,1,150
-3*exp(x)*sin(y)*cos(y) /(2-exp(x))
area( power(x,0.1)*(1.2-x)*(1-exp(20*(x-1))) ; exp(-power(x,2))-0.2 )
power(x,0.1)*(1.2-x)*(1-exp(20*(x-1)))
exp(-power(x,2))-0.2
0.026*(1- y/12000)*y
