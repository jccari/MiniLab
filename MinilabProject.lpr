program MinilabProject;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, mainwindow, cmdbox, FramePlot, tachartlazaruspkg,
  { extra tool methods }
  DataTypes,
  syntacticAnalyzer,
  ConversionStrMatrix,
  { root methods }
  mOpenMethod,
  ClosedMethods,
  { interpolation method}
  interpolationMethods,
  { intersection functions}
    FunctionOperations,
  { integral and area methods}
  TrapezoidalMethod,
  Simpson1_3Method,
  Simpson3_8Method,
  { edo methods }
  EulerMethod,
  HeunMethod,
  RungeKMethod,
  DormandPMethod,

  ExtrapolationMethods;

{$R *.res}

begin
  RequireDerivedFormResource:=True;
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.

