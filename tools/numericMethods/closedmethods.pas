unit ClosedMethods;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Dialogs,math ,ParseMath, DataTypes;
type
    Tarraystring = array of string;
type
  TClosedMethods = class
    private

    public
      //fun : TParseMath; //* math function
      constructor create();
      function bisectionMethod(a: Double; b:Double; e: Double; FExpression: string): TResult;
      function fakePositionMethod(a: double; b: double; e: double; FExpression: string): TResult;
  end;

//var fun: TParseMath; //* math function

implementation

//constructor TClosedMethods.create( fExpresion: string);
constructor TClosedMethods.create();
begin
end;

function TClosedMethods.bisectionMethod(a: Double; b:Double; e: Double; FExpression: string ): TResult ;
var
  fun: TParseMath;
  eAbs: Double; // absolute error
  xn: Double; // xr
  xn_1: Double; // last xr
  bolzano: Double;
  fxn: Double;
  fa,fb: Double;
  signo: Double;
begin
    Result := TResult.Create;

    fun := TParseMath.create();
    fun.Expression:=FExpression;
    fun.AddVariable('x',0); fun.Evaluate();
  //Bisection method
    eAbs:= 1000000; // this is a trick, only for the first iteration
    //xnOld:= xn;
    xn:= ((a+b)/2);
    fun.NewValue('x',a); fa := fun.Evaluate();
    fun.NewValue('x',b); fb := fun.Evaluate();
    fun.NewValue('x',xn); fxn := fun.Evaluate();
    signo := fa*fxn;
    // Comprobamos bolzano
    if( fa=infinity) or (fxn=infinity) then
    begin
         Result.result := 'NC'; // No Continuo -> no cumple bolzano
         exit;
    end;
    //guardamos en la matriz Result.matrix..matrix
    Result.matrix.AList.Add( FloatToStr(a) );
    Result.matrix.BList.Add( FloatToStr(b) );
    Result.matrix.XnList.Add( FloatToStr(xn) );
    Result.matrix.SignList.Add( FloatToStr(signo) );
    Result.matrix.ErrorList.Add( '-' );
    //////

    bolzano:= fa*fb;
    if ( bolzano> 0) then
    begin
       Result.result := FloatToStr(fa)+' , '+ FloatToStr(fb)+' No cumple teorema de bolzano';
       exit;
    end
    else
    begin
        if( bolzano = 0) then
        begin
            if(fa=0) then begin
              Result.result := FloatToStr(a);// +' es la solucion 1'
              Exit;
            end
            else
            begin
                if( fb=0) then begin
                    Result.result := FloatToStr(b);//+' es la solucion 2'
                    Exit;
                end
                else
                    if (fxn=0) then begin
                    Result.result := FloatToStr(xn);// +' es la solucion 3' ;
                    Exit;
                    end;
            end;
        end
        else // bisection method
        begin
          while( (e<eAbs) and (fa<>0) ) do
          begin

            //signo := fa*fxn;
            if (signo = 0) then begin
               Result.result:=FloatToStr(xn);
               exit
            end;

            if( signo <0) then
                b := xn
            else
                a:=xn;
            xn_1:= xn;
            xn:= (a+b)/2 ;
            fun.NewValue('x',a); fa := fun.Evaluate();
            fun.NewValue('x',xn); fxn := fun.Evaluate();
            signo := fa*fxn;
            eAbs:= abs(xn - xn_1);

            if( fa=infinity) or (fxn=infinity) then
            begin
                Result.result := 'NC'; // No Continuo -> no cumple bolzano
                exit;
            end;

            //guardamos en la matriz Result.matrix..matrix

            Result.matrix.AList.Add( FloatToStr(a) );
            Result.matrix.BList.Add( FloatToStr(b) );
            Result.matrix.XnList.Add( FloatToStr(xn) );
            Result.matrix.SignList.Add( FloatToStr(signo) );
            Result.matrix.ErrorList.Add( FloatToStr(eAbs) );

            end;
        end;

    end;
    //Result.matrix..result:=Result.matrix..result + FloatToStr(xn);
    Result.result:= FloatToStr(xn);
end;

function TClosedMethods.fakePositionMethod(a: Double; b:Double; e: Double; FExpression: string ): TResult ;
var
  fun: TParseMath;
  eAbs: Double; // absolute error
  xn: Double; // xr
  xn_1: Double; // last xr
  bolzano: Double;
  fxn: Double;
  fa,fb: Double;
  signo: Double;
begin
    Result := TResult.Create;

    fun := TParseMath.create();
    fun.Expression:=FExpression;
    fun.AddVariable('x',0); fun.Evaluate();

    eAbs:= 1000000; // this is a trick, only for the first iteration

    fun.NewValue('x',a); fa := fun.Evaluate();
    fun.NewValue('x',b); fb := fun.Evaluate();
    fun.NewValue('x',xn); fxn := fun.Evaluate();
    xn:= (a-( (fa*(b-a))/(fb-fa) ) );
    signo := fa*fxn;

    // Comprobamos bolzano
    if( fa=infinity) or (fxn=infinity) then
    begin
         Result.result := 'NC'; // No Continuo -> no cumple bolzano
         exit;
    end;
    //guardamos en la matriz Result.matrix..matrix
    Result.matrix.AList.Add( FloatToStr(a) );
    Result.matrix.BList.Add( FloatToStr(b) );
    Result.matrix.XnList.Add( FloatToStr(xn) );
    Result.matrix.SignList.Add( FloatToStr(signo) );
    Result.matrix.ErrorList.Add( '-' );
    bolzano:= fa*fb;
    if ( bolzano> 0) then
    begin
       Result.result := 'No cumple teorema de bolzano';// FloatToStr(fa)+' , '+ FloatToStr(fb)+' No cumple teorema de bolzano';
       exit;
    end
    else
    begin
        if( bolzano = 0) then
        begin
            if(fa=0) then
              Result.result := FloatToStr(a)// +' es la solucion 1'
            else
            begin
                if( fb=0) then
                    Result.result := FloatToStr(b) //+' es la solucion 2'
                else
                    if (fxn=0) then
                    Result.result := FloatToStr(xn);// +' es la solucion 3' ;
            end;
        end
        else // bisection method
        begin
          while( (e<eAbs) and (fa<>0) ) do
          begin
            //signo := fa*fxn;
            if( signo <0) then
                b := xn
            else
                a:=xn;
            xn_1:= xn;
            fun.NewValue('x',a); fa := fun.Evaluate();
            fun.NewValue('x',b); fb := fun.Evaluate();
            fun.NewValue('x',xn); fxn := fun.Evaluate();
            xn:= (a-( (fa*(b-a))/(fb-fa) ) );
            signo := fa*fxn;
            eAbs:= abs(xn - xn_1);

            if( fa=infinity) or (fxn=infinity) then
            begin
                Result.result := 'Funcion No Continua'; // No Continuo -> no cumple bolzano
                exit;
            end;

            //guardamos en la matriz Result.matrix..matrix

            Result.matrix.AList.Add( FloatToStr(a) );
            Result.matrix.BList.Add( FloatToStr(b) );
            Result.matrix.XnList.Add( FloatToStr(xn) );
            Result.matrix.SignList.Add( FloatToStr(signo) );
            Result.matrix.ErrorList.Add( FloatToStr(eAbs) );

            end;
        end;

    end;
    Result.result:= FloatToStr(xn);
    Result.thereAreResult:= True;
end;

end.

