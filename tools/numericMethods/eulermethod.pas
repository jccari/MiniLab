unit EulerMethod;

{$mode objfpc}{$H+}

interface

uses
  Classes,sysutils , BaseMethod, ParseMath, DataTypes;

type
   TEulerMethod = class(TBaseMethod)
   public
      function solve() : TResult; override;
      function evaluar(valorX, valorY : Real ; f_x : String ) : Real;
   end;

implementation

function TEulerMethod.evaluar(valorX, valorY : Real ; f_x : String ) : Real;
var
   MiParse : TParseMath;
begin
  try
  Miparse := TParseMath.create();
  MiParse.AddVariable('x',valorX);
  MiParse.AddVariable('y',valorY);
  MiParse.Expression:= f_x;
  evaluar := MiParse.Evaluate();
  except
     evaluar:=0.0;
     Exit;
  end;
end;

function TEulerMethod.solve() : TResult;
var
   i : Integer;
   xi, yi : Double;
   xnlist, ynlist: TStringList;
begin
   Result := TResult.Create;
   xnlist := TStringList.Create;
   ynlist := TStringList.Create;

   xnlist.Add( FloatToStr(_x0) );
   ynlist.Add( FloatToStr(_y0) );

   for i:= 1 to N do begin
     xi := StrToFloat( xnlist[ xnlist.Count -1 ] );
     yi := StrToFloat( ynlist[ xnlist.Count -1 ] );

     xnlist.Add( FloatToStr( xi+ _h ) );
     ynlist.Add( FloatToStr( yi + _h * evaluar(xi,yi,_f) ) );
   end;

   Result.matrix.XnList := xnlist;
   Result.matrix.YnList := ynlist;

end;

end.
