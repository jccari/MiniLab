unit TrapezoidalMethod;

{$mode objfpc}{$H+}

interface

uses
   math, sysutils, ParseMath ,DataTypes;

type
   TArrStr = array of string;

   TTrapezoidalMethod = class
   private
      _low : real;
      _high : real;
      _n : integer;
      _f : String;
      function evaluar(valorX: Real ; f_x : String ) : Real;
   public
      constructor create();
      destructor destroy(); override;
      procedure setParameters( func: String; low, high: Real; n:Integer);
      function eval() : TResult;
   end;

implementation

constructor TTrapezoidalMethod.create();
begin

end;

destructor TTrapezoidalMethod.destroy();
begin

end;

procedure TTrapezoidalMethod.setParameters( func: String; low, high: Real; n:Integer);
begin
   _f := func;
   _low := low;
   _high := high;
   _n := n;
end;

function TTrapezoidalMethod.eval() : TResult;
var
   h, xi, sum, aux : real;
   i : integer;
begin
   Result := TResult.Create;
   h := (_high - _low) / _n;
   sum := 0;
   for i := 1 to _n - 1 do
   begin
      xi := _low + i * h;
      sum := sum + evaluar( xi,_f);
   end;

   aux := evaluar( _low, _f) + evaluar( _high, _f);
   aux := aux / 2 + sum;
   aux := aux * h;

   Result.result := FloatToStr( aux );
end;

function TTrapezoidalMethod.evaluar(valorX: Real ; f_x : String ) : Real;
var
   MiParse : TParseMath;
begin
  try
    Miparse := TParseMath.create();
    MiParse.AddVariable('x',valorX);
    MiParse.Expression:= f_x;
    evaluar := MiParse.Evaluate();
  except
     evaluar:=0.0001;
     Exit;
  end;
end;


end.
