unit Simpson3_8Method;

{$mode objfpc}{$H+}

interface

uses
    ParseMath, strutils, sysutils, DataTypes;

type
   TSimpson3_8Method = class
   private
      _function : TParseMath;
      _low : real;
      _high : real;
      _n : integer;
      _f : String;
      function evaluar(valorX: Real ; f_x : String ) : Real;
   public
      constructor create();
      destructor destroy(); override;
      procedure setParameters( func: String; low, high: Real; n:Integer);
      function eval() : TResult;
   end;

implementation

constructor TSimpson3_8Method.create();
begin
   _function := TParseMath.create();
end;

destructor TSimpson3_8Method.destroy();
begin
   _function.destroy();
end;

procedure TSimpson3_8Method.setParameters( func: String; low, high: Real; n:Integer);
begin
   _f := func;
   _low := low;
   _high := high;
   _n := n;
end;

// 0.4*power(2.718281828459045235360,-power(x,2)/2)

// control
 // (1/sqrt(2*3.14159265358979323846))*power(2.718281828459045235360,-0.5*power(x,2))
function TSimpson3_8Method.eval() : TResult;
var
   h, xi, sum1, sum2, sum3, aux : real;
   i : integer;
begin
   Result := TResult.Create;
   h := (_high - _low) / _n;
   sum1 := 0;
   sum2 := 0;
   sum3 := 0;

   xi := _low +h ;
   while xi < _high do
   begin
      sum1 := sum1 + evaluar(xi, _f);
      xi := xi + (3*h);
   end;

   xi:= _low +(2*h);
   while xi < _high do
   begin
      sum2 := sum2 + evaluar(xi, _f);
      xi := xi + (3*h);
   end;

   xi:= _low +(3*h);
   while xi < _high do
   begin
      sum3 := sum3 + evaluar( xi, _f);
      xi := xi + (3*h);
   end;

   aux := evaluar( _low, _f);
   aux := aux + evaluar( _high, _f);
   aux := aux + (3 * sum1) + (3 * sum2) + (2 * sum3);
   aux := aux * (3 * h / 8);

   Result.result := FloatToStr( aux );
end;

function TSimpson3_8Method.evaluar(valorX: Real ; f_x : String ) : Real;
var
   MiParse : TParseMath;
begin
  try
    Miparse := TParseMath.create();
    MiParse.AddVariable('x',valorX);
    MiParse.Expression:= f_x;
    evaluar := MiParse.Evaluate();
  except
     evaluar:=0.0001;
     Exit;
  end;
end;

end.
