unit mOpenMethod;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, strutils, DataTypes, parsemath;
type
  TOpenMethod = class
    private
      function evaluar(valorX : Real ; f_x : String ) : Real;
      function getPresicion(error: Double): Integer;
      function getZerosStr(precision: integer): string;

    public
      function newton(x: Double; funExpression, funDExpression: string; e: double): TResult;
      function secante(x: Double; funExpression: string; e: double): TResult;


  end;

implementation

function TOpenMethod.evaluar(valorX : Real ; f_x : String ) : Real;
var
   MiParse : TParseMath;
begin
  try
   Miparse := TParseMath.create();
   MiParse.AddVariable('x',valorX);
   MiParse.Expression:= f_x;
   evaluar := MiParse.Evaluate();
  except
     evaluar:=0.0001;
     Exit;
  end;
end;

function TOpenMethod.newton(x: Double; funExpression, funDExpression: string; e: double): TResult;
var
  fun, fund: TParseMath;
  xn,xn_1,eAbs, fxn, fdxn: Double;
  ePresicionStr: string;
begin
  Result := TResult.Create;

  fun:= TParseMath.create();
  fund:= TParseMath.create();

  fun.Expression:=funExpression;
  fund.Expression:=funDExpression;

  fun.AddVariable('x',0); fun.Evaluate();
  fund.AddVariable('x',0); fund.Evaluate();

  ePresicionStr := getZerosStr(getPresicion(e));
  eAbs:= 10000000;
  xn_1:= x;

  Result.matrix.XnList.Add( FloatToStr(xn_1) );
  Result.matrix.ErrorList.Add('-');

  while (e < eAbs) do begin

    fund.NewValue('x',xn_1); fdxn := fund.Evaluate();
    if (fdxn = 0) then
    begin // verificamos que la tangente sea != 0
      xn_1 := xn_1 - e ; // si lo es -> perturbamos xn_1 con el error
      fund.NewValue('x',xn_1); fdxn := fund.Evaluate();
    end;

    fun.NewValue('x',xn_1); fxn := fun.Evaluate();


    xn :=  xn_1 +(-1*(fxn /fdxn) );

    eAbs:= abs(xn- xn_1);

    Result.matrix.XnList.Add( FloatToStr(xn_1) );
    Result.matrix.ErrorList.Add( FloatToStr(eAbs) );

    xn_1:= xn;
  end;

  //Result.result:= FloatToStr(xn);
  Result.result:= FormatFloat( ePresicionStr , xn);
end;

function TOpenMethod.secante(x: Double; funExpression: string; e: double): TResult;
var
  xn,xn_1,eAbs, fxn, fxph, fxmh, h: Double;

  derivada:double;
begin
  Result := TResult.Create;

  eAbs:= 10000000;
  h := e/2;
  xn_1:= x;

  Result.matrix.XnList.Add( FloatToStr(xn_1) );
  Result.matrix.ErrorList.Add('-');

  while (e < eAbs) do begin

    fxn := evaluar(xn_1,funExpression);
    fxph := evaluar(xn_1+h,funExpression);
    fxmh := evaluar(xn_1-h,funExpression);

    derivada := (fxph-fxmh)/2*h;

    if (derivada=0) then
    begin // verificamos que la tangente sea != 0
      xn_1 := xn_1 - e ; // si lo es -> perturbamos xn_1 con el error
      fxn := evaluar(xn_1,funExpression);
      fxph := evaluar(xn_1+h,funExpression);
      fxmh := evaluar(xn_1-h,funExpression);
    end;

    xn :=  xn_1 -( (2*h*fxn)/(fxph-fxmh) );

    eAbs:= abs(xn- xn_1);

    Result.matrix.XnList.Add( FloatToStr(xn_1) );
    Result.matrix.ErrorList.Add( FloatToStr(eAbs) );

    xn_1:= xn;
  end;

  Result.result:= FloatToStr(xn);
end;

/////////////////////////
function TOpenMethod.getPresicion(error: Double): Integer;
var
  eString: string;
begin
     eString:= FloatToStr(error);

     Result := AnsiPos('1',eString)-AnsiPos('.',eString)-1;
end;

//DupeString function needs srtutils library
function TOpenMethod.getZerosStr(precision: Integer): string;
begin
  Result := DupeString('0',precision);
  Result := '0.'+Result;
end;

end.

