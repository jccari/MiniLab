unit BaseMethod;

{$mode objfpc}{$H+}

interface

uses
   DataTypes;

type
   TArrxy = array of array of real;

   TBaseMethod = class
   protected
      _x0 : real;
      _y0 : real;
      _xf : real;
      _h : real;
      N : Integer;
      _f : String;
   public
      constructor create();
      destructor destroy(); override;

      procedure setData(func : string; x0, y0, xf : Double; _n: Integer);
      //function solve() : TArrxy; virtual; abstract;
      function solve() : TResult; virtual; abstract;
   end;

implementation

constructor TBaseMethod.create();
begin

end;

destructor TBaseMethod.destroy();
begin

end;

//procedure TBaseMethod.setData(func : string; x0, y0, xf: Double);
procedure TBaseMethod.setData(func : string; x0, y0, xf: Double; _n: Integer);
begin
   _f := func;
   _x0 := x0;
   _y0 := y0;
   _xf := xf;
   //N := trunc((_xf-_x0)*(3/0.75) );
   //N := _n;
   //N := trunc((_xf-_x0)*10 );
   //_h := ( _xf-_x0)/N;
   _h := 0.001;
   N := trunc((_xf-_x0)*_h );

   //_h := abs(_x0-_y0)/13;
   //_h := 0.3;
   //ShowMessage(FloatToStr(_h));
end;

end.
