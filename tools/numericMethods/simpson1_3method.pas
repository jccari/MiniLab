unit Simpson1_3Method;

{$mode objfpc}{$H+}

interface

uses
   ParseMath, strutils, sysutils, DataTypes;

type
   TArrStr = array of string;

   TSimpson1_3Method = class
   public
      _function : TParseMath;
      _low : real;
      _high : real;
      _n : integer;
      _f :String;
   public
      constructor create();
      destructor destroy(); override;
      procedure setParameters( func: String; low, high: Real; n:Integer);
      function eval() : TResult;
      function evaluar(valorX : Real ; f_x : String ) : Real;
   end;

implementation

constructor TSimpson1_3Method.create();
begin
  _function := TParseMath.create();
end;

destructor TSimpson1_3Method.destroy();
begin
 _function.destroy ;
end;

procedure TSimpson1_3Method.setParameters( func: String; low, high: Real; n:Integer);
begin
   _f := func;
   _low := low;
   _high := high;
   _n := n;
end;

// 0.4*power(e,-power(x,2)/2)
// 0.4*power(2.718281828459045235360,-power(x,2)/2)
// power(2.718281828459045235360,2*x)-x
function TSimpson1_3Method.eval() : TResult;
var
   h, xi, sumOdds, sumEvens, aux : double;
   i : real;
   auxstr: String;
begin
   Result := TResult.Create;
   h := (_high - _low) / (2*_n);
   sumEvens := 0;
   sumOdds := 0;
   xi := _low + (2*h);
   while( xi < _high ) do
   begin
      sumEvens := sumEvens + evaluar(xi,_f);
      xi := xi + (2*h);
   end;

   xi := _low + h;
   while( xi < _high ) do
   begin
      sumOdds := sumOdds + evaluar(xi,_f);
      auxstr := auxstr + FloatToStr(xi) + ' ';
      xi := xi + (2*h);
   end;

   aux := evaluar(_low,_f);
   aux := aux + evaluar(_high,_f);
   aux := aux + 2 * sumEvens + 4 * sumOdds;
   aux := aux * h / 3;

   Result.result := FloatToStr( aux );
end;

function TSimpson1_3Method.evaluar(valorX: Real ; f_x : String ) : Real;
var
   MiParse : TParseMath;
begin
  try
    Miparse := TParseMath.create();
    MiParse.AddVariable('x',valorX);
    MiParse.Expression:= f_x;
    evaluar := MiParse.Evaluate();
  except
     evaluar:=0;
     Exit;
  end;
end;


end.

