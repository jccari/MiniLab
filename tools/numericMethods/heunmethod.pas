unit HeunMethod;

{$mode objfpc}{$H+}

interface

uses
    BaseMethod, ParseMath, DataTypes, Classes,sysutils;

type
   THeunMethod = class(TBaseMethod)
   public
      //function solve() : TArrxy; override;
      function solve() : TResult; override;
      function evaluar(valorX, valorY : Real ; f_x : String ) : Real;
   end;

implementation

function THeunMethod.evaluar(valorX, valorY : Real ; f_x : String ) : Real;
var
   MiParse : TParseMath;
begin
  try
  Miparse := TParseMath.create();
  MiParse.AddVariable('x',valorX);
  MiParse.AddVariable('y',valorY);
  MiParse.Expression:= f_x;
  evaluar := MiParse.Evaluate();
  except
     evaluar:=0;
     Exit;
  end;
end;

// control
// (10-y)*0.5*(power(2.7182818,x)+power(2.7182818,-x))
// (10-y)*cosh(x)

function THeunMethod.solve() : TResult;
var
   i : integer;
   xi, yi : real;
   m, yT, fT : real;
   xnlist, ynlist: TStringList;
begin
   Result := TResult.Create;
   xnlist := TStringList.Create;
   ynlist := TStringList.Create;

   xnlist.Add( FloatToStr(_x0) );
   ynlist.Add( FloatToStr(_y0) );

   for i:= 1 to N do begin
      xi := StrToFloat( xnlist[ xnlist.Count -1 ] );
      yi := StrToFloat( ynlist[ xnlist.Count -1 ] );

      fT := evaluar(xi,yi,_f);
      yT := yi + _h * fT;

      m := fT + evaluar(xi+_h,yT,_f);
      m := m / 2;

      xnlist.Add( FloatToStr( xi+ _h ) );
      ynlist.Add( FloatToStr( yi + _h * m ) );

   end;

   Result.matrix.XnList := xnlist;
   Result.matrix.YnList := ynlist;

end;

end.
