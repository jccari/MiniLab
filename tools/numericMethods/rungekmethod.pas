unit RungeKMethod;

{$mode objfpc}{$H+}

interface

uses
    BaseMethod,ParseMath, DataTypes,Classes,sysutils;

type
   TRungeKuttaMethod = class(TBaseMethod)
   public
      function solve() : TResult; override;
      function evaluar(valorX, valorY : Real ; f_x : String ) : Real;
   end;

// (sin(power(2.718281,x*y)))/(2*y-x*cos(power(2.718281,x*y)))
// f(-0.7) = 0.49

implementation


function TRungeKuttaMethod.solve() : TResult;
var
   i : integer;
   xi, yi : real;
   h_2, k1, k2, k3 , k4, m : real;
   xnlist, ynlist: TStringList;
begin
   Result := TResult.Create;
   xnlist := TStringList.Create;
   ynlist := TStringList.Create;

   xnlist.Add( FloatToStr(_x0) );
   ynlist.Add( FloatToStr(_y0) );

   h_2 := _h / 2;
   for i:= 1 to N do begin
      xi := StrToFloat( xnlist[ xnlist.Count -1 ] );
      yi := StrToFloat( ynlist[ xnlist.Count -1 ] );

      k1 := evaluar(xi,yi,_f);
      k2 := evaluar(xi +h_2 , yi + k1 * h_2,_f);
      k3 := evaluar(xi +h_2 , yi + k2 * h_2 ,_f);
      k4 := evaluar(xi +_h , yi + k3 * _h ,_f);
      m := (k1 + 2*k2 + 2*k3 + k4) / 6;

      xnlist.Add( FloatToStr( xi+ _h ) );
      ynlist.Add( FloatToStr( yi + _h * m ) );

   end;

   Result.matrix.XnList := xnlist;
   Result.matrix.YnList := ynlist;

end;

function TRungeKuttaMethod.evaluar(valorX, valorY : Real ; f_x : String ) : Real;
var
   MiParse : TParseMath;
begin
  try
  Miparse := TParseMath.create();
  MiParse.AddVariable('x',valorX);
  MiParse.AddVariable('y',valorY);
  MiParse.Expression:= f_x;
  evaluar := MiParse.Evaluate();
  except
     evaluar:=0.0;
     Exit;
  end;
end;
end.
