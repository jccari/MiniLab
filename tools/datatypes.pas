unit DataTypes;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

const
  DEFAULT_ERROR = 0.0001;
  { for graphics, DEFAULT VALUES }
  DEFAULT_H = 0.1;
  DEFAULT_XMIN = -10;
  DEFAULT_XMAX = 10;
  DEFAULT_YMIN = -10;
  DEFAULT_YMAX = 10;

const
  ERROR_BAD_ARGS = '[ERROR] Bad arguments';
  ERROR_DECLARE_VAR = '[ERROR] Can not declare variable';
  ERROR_PLOT = '[ERROR] Can not plot function';
  ERROR_INTERSECTION = '[ERROR] Can not intersect';
  ERROR_INTEGRATE = '[ERROR] Can not integrate';
  ERROR_FINDALLROOTS = '[ERROR] Can not find all roots';
  ERROR_FINDROOT = '[ERROR] Can not find root';
  ERROR_OPERATION = '[ERROR] Can not do this operation';
  ERROR_EXTRAPOLATION = '[ERROR] Can not extrapolate';
  ERROR_EVALUATE_XVALUE = '[ERROR] Can not evaluate';
  ERROR_EDO = '[ERROR] Can not resolve EDO';
  ERROR_AREA = '[ERROR] Can not calculate area';

  // Constants for Number of operation arguments
const
  ARGS_NEW_VARIABLE = 1;
  ARGS_PLOT = 3;
  ARGS_INTERSECTION = 6;
  ARGS_INTEGRATE = 3;
  ARGS_AREA = 3;
  ARGS_ROOT_OPENMETHOD_SECANTE = 3;
  ARGS_ROOT_OPENMETHOD_NEWTON = 4;
  ARGS_ROOT_CLOSEDMETHODS = 4;

// Constant for OPERATION
const
  BAD_ARGUMENTS = -1;
  OPERATION_UNKNOWN = 0;
  NEW_VARIABLE = 1;
  NEW_MATRIX = 14;
  OPERATION_PLOT = 2;
  OPERATION_INTERSECTION = 3;
  OPERATION_INTEGRATE = 4;
  OPERATION_AREA = 5;

  OPERATION_FINDALLROOTS = 6;
  OPERATION_FINDROOT_OPENMETHODS = 7;
  OPERATION_FINDROOT_CLOSEDMETHODS = 8;
  OPERATION_FINDROOT = 9;

  OPERATION_MATRIX = 10;
  OPERATION_MATH = 11;

  OPERATION_INTERPOLATION = 12;
  OPERATION_EDO = 13;
  OPERATION_EXTRAPOLATION = 15;
  OPERATION_EVALUATE_XVALUE = 16;

  CLEAR = 20;

type
  t_matrix= array of array of real;

type
  TResultAnalyzer = class
    public
      operation,
      numArgs: Integer;
      variableName,
      functionName : String;
      args : TStringList;
      constructor Create;
  end;

type
  TResultMatrix = class
    public
      AList,
      BList,
      XnList,
      YnList,
      SignList,
      ErrorList,
      IntersectionList : TStringList;
      constructor Create;
  end;

type
  TResult = class
    public
      result : String;
      matrix : TResultMatrix;
      thereAreResult: Boolean;
      { for extrapolation }
      m, b, r, r2 : String;
      constructor Create;
  end;


implementation

constructor TResultAnalyzer.Create;
begin
  args := TStringList.Create;
end;

constructor TResult.Create;
begin
  matrix := TResultMatrix.Create;
  thereAreResult := False;
end;

constructor TResultMatrix.Create;
begin
  AList := TStringList.Create;
  BList := TStringList.Create;
  XnList := TStringList.Create;
  YnList := TStringList.Create;
  SignList := TStringList.Create;
  ErrorList := TStringList.Create;
  IntersectionList := TStringList.Create;
end;

end.

