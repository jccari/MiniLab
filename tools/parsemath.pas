unit ParseMath;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, math, fpexprpars, Dialogs, ConversionStrMatrix, mMatrix, DataTypes;

type
  TParseMath = Class

  Private
      FParser: TFPExpressionParser;
      identifier: array of TFPExprIdentifierDef;
      Procedure AddFunctions();


  Public

      Expression: string;
      function NewValue( Variable:string; Value: Double ): Double;
      procedure AddVariable( Variable: string; Value: Double );
      procedure AddString( Variable: string; Value: string );
      function Evaluate(  ): Double;
      function EvaluateStr(): String;
      constructor create();
      destructor destroy;

  end;

implementation



constructor TParseMath.create;
begin
   FParser:= TFPExpressionParser.Create( nil );
   FParser.Builtins := [ bcMath ];
   AddFunctions();

end;

destructor TParseMath.destroy;
begin
    FParser.Destroy;
end;

function TParseMath.NewValue( Variable: string; Value: Double ): Double;
begin
    FParser.IdentifierByName(Variable).AsFloat:= Value;

end;

function TParseMath.Evaluate(): Double;
begin
     FParser.Expression:= Expression;
     Result:= ArgToFloat( FParser.Evaluate );
end;

function TParseMath.EvaluateStr(): String;
begin
     FParser.Expression:= Expression;
     Result:= FParser.Evaluate.ResString ;
end;

function IsNumber(AValue: TExprFloat): Boolean;
begin
  result := not (IsNaN(AValue) or IsInfinite(AValue) or IsInfinite(-AValue));
end;



procedure ExprFloor(var Result: TFPExpressionResult; Const Args: TExprParameterArray); // maximo entero
var
  x: Double;
begin
   x := ArgToFloat( Args[ 0 ] );
   if x > 0 then
     Result.ResFloat:= trunc( x )

   else
     Result.ResFloat:= trunc( x ) - 1;

end;

Procedure ExprTan( var Result: TFPExpressionResult; Const Args: TExprParameterArray);
var
  x: Double;
begin
   x := ArgToFloat( Args[ 0 ] );
   if IsNumber(x) and ((frac(x - 0.5) / pi) <> 0.0) then
      Result.resFloat := tan(x)

   else
     Result.resFloat := NaN;
end;
{
Procedure ExprNewton( var Result: TFPExpressionResult; Const Args: TExprParameterArray);
var
  x: Double;
  f: string;
  TheNewton: TNewton;
begin
   f:= Args[ 0 ].ResString;
   x:= ArgToFloat( Args[ 1 ] );

   TheNewton:= TNewton.Create;
   TheNewton.InitialPoint:= x;
   TheNewton.Expression:= f;
   Result.ResFloat := TheNewton.Execute;

   TheNewton.Destroy;

end;
}
Procedure ExprSin( var Result: TFPExpressionResult; Const Args: TExprParameterArray);
var
  x: Double;
begin
   x := ArgToFloat( Args[ 0 ] );
   Result.resFloat := sin(x)

end;

Procedure ExprCos( var Result: TFPExpressionResult; Const Args: TExprParameterArray);
var
  x: Double;
begin
   x := ArgToFloat( Args[ 0 ] );
   Result.resFloat := cos(x)

end;

Procedure ExprLn( var Result: TFPExpressionResult; Const Args: TExprParameterArray);
var
  x: Double;
begin
    x := ArgToFloat( Args[ 0 ] );
   if IsNumber(x) and (x > 0) then
      Result.resFloat := ln(x)

   else
     Result.resFloat := NaN;

end;

Procedure ExprLog( var Result: TFPExpressionResult; Const Args: TExprParameterArray);
var
  x: Double;
begin
    x := ArgToFloat( Args[ 0 ] );
   if IsNumber(x) and (x > 0) then
      Result.resFloat := ln(x) / ln(10)

   else
     Result.resFloat := NaN;

end;

Procedure ExprSQRT( var Result: TFPExpressionResult; Const Args: TExprParameterArray);
var
  x: Double;
begin
    x := ArgToFloat( Args[ 0 ] );
   if IsNumber(x) and (x > 0) then
      Result.resFloat := sqrt(x)

   else
     Result.resFloat := NaN;

end;

Procedure ExprPower( var Result: TFPExpressionResult; Const Args: TExprParameterArray);
var
  x,y: Double;
begin
    x := ArgToFloat( Args[ 0 ] );
    y := ArgToFloat( Args[ 1 ] );

    Result.resFloat := power(x,y);
end;

Procedure ExprSinh( var Result: TFPExpressionResult; Const Args: TExprParameterArray);
var
  x: Double;
begin
   x := ArgToFloat( Args[ 0 ] );
   Result.ResFloat:= 0.5*( exp(x)- exp(-x) );
end;

Procedure ExprCosh( var Result: TFPExpressionResult; Const Args: TExprParameterArray);
var
  x: Double;
begin
   x := ArgToFloat( Args[ 0 ] );
   Result.ResFloat:= 0.5*( exp(x)+ exp(-x) );
end;

Procedure ExprTanh( var Result: TFPExpressionResult; Const Args: TExprParameterArray);
var
  x: Double;
begin
   x := ArgToFloat( Args[ 0 ] );
   Result.ResFloat:= sinh(x)/cosh(x);
end;

Procedure ExprSumMatrix( var Result: TFPExpressionResult; Const Args: TExprParameterArray);
var
  m1Str, m2Str: String;
  m1Real, m2Real, mResReal: t_matrix;
  conv : TConversion;
  matrix : TMatrix;
begin
   conv := TConversion.Create;
   matrix:= TMatrix.create();

   m1Str := Args[ 0 ].ResString ;
   m2Str := Args[ 1 ].ResString ;

   m1Real := conv.StrToMatrix( m1Str );
   m2Real := conv.StrToMatrix( m2Str );

   if ( Length(m1Real) <> Length(m2Real)) or ( Length(m1Real[0]) <> Length(m2Real[0])) then
      Result.ResString:= 'ERROR: las matrices deben ser de igual dimension';

   mResReal := matrix.add( m1Real, m2Real );

   Result.ResString:= conv.MatrixToStr( mResReal );
end;

Procedure ExprSubtractMatrix( var Result: TFPExpressionResult; Const Args: TExprParameterArray);
var
  m1Str, m2Str: String;
  m1Real, m2Real, mResReal: t_matrix;
  conv : TConversion;
  matrix : TMatrix;
begin
   conv := TConversion.Create;
   matrix:= TMatrix.create();

   m1Str := Args[ 0 ].ResString ;
   m2Str := Args[ 1 ].ResString ;

   m1Real := conv.StrToMatrix( m1Str );
   m2Real := conv.StrToMatrix( m2Str );

   if ( Length(m1Real) <> Length(m2Real)) or ( Length(m1Real[0]) <> Length(m2Real[0])) then
      Result.ResString:= 'ERROR: las matrices deben ser de igual dimension';

   mResReal := matrix.subtract( m1Real, m2Real );

   Result.ResString:= conv.MatrixToStr( mResReal );
end;

Procedure ExprMultiplyMatrix( var Result: TFPExpressionResult; Const Args: TExprParameterArray);
var
  m1Str, m2Str: String;
  m1Real, m2Real, mResReal: t_matrix;
  conv : TConversion;
  matrix : TMatrix;
begin
   conv := TConversion.Create;
   matrix:= TMatrix.create();

   m1Str := Args[ 0 ].ResString ;
   m2Str := Args[ 1 ].ResString ;

   m1Real := conv.StrToMatrix( m1Str );
   m2Real := conv.StrToMatrix( m2Str );

   if ( Length(m1Real[0]) <> Length(m2Real)) then
      Result.ResString:= 'ERROR: columna de M1 debe ser igual a filas de M2';

   mResReal := matrix.multiply( m1Real, m2Real );

   Result.ResString:= conv.MatrixToStr( mResReal );
end;

Procedure ExprInverseMatrix( var Result: TFPExpressionResult; Const Args: TExprParameterArray);
var
  m1Str : String;
  m1Real, mResReal: t_matrix;
  conv : TConversion;
  matrix : TMatrix;
begin
   conv := TConversion.Create;
   matrix:= TMatrix.create();

   m1Str := Args[ 0 ].ResString ;

   m1Real := conv.StrToMatrix( m1Str );

   if ( matrix.det(m1Real) = 0 ) then
      Result.ResString:= 'ERROR: No existe la matriz inversa, determinante es 0';

   mResReal := matrix.inverse( m1Real);

   Result.ResString:= conv.MatrixToStr( mResReal );
end;

Procedure ExprDeterminatMatrix( var Result: TFPExpressionResult; Const Args: TExprParameterArray);
var
  m1Str : String;
  m1Real : t_matrix;
  conv : TConversion;
  matrix : TMatrix;
  res : Double;
begin
   conv := TConversion.Create;
   matrix:= TMatrix.create();

   m1Str := Args[ 0 ].ResString ;

   m1Real := conv.StrToMatrix( m1Str );

   res := matrix.det(m1Real);

   Result.ResString:= FloatToStr( res );
end;

Procedure ExprTransposedMatrix( var Result: TFPExpressionResult; Const Args: TExprParameterArray);
var
  m1Str: String;
  m1Real, mResReal: t_matrix;
  conv : TConversion;
  matrix : TMatrix;
begin
   conv := TConversion.Create;
   matrix:= TMatrix.create();

   m1Str := Args[ 0 ].ResString ;

   m1Real := conv.StrToMatrix( m1Str );

   mResReal := matrix.transposed(m1Real);

   Result.ResString:= conv.MatrixToStr( mResReal );
end;

Procedure ExprMultiplyByNumberMatrix( var Result: TFPExpressionResult; Const Args: TExprParameterArray);
var
  m1Str : String;
  m1Real, mResReal: t_matrix;
  num : Double;
  conv : TConversion;
  matrix : TMatrix;
begin
   conv := TConversion.Create;
   matrix:= TMatrix.create();

   num := ArgToFloat( Args[ 0 ] ) ;
   m1Str := Args[ 1 ].ResString ;

   m1Real := conv.StrToMatrix( m1Str );

   mResReal := matrix.multiplyByNumber(m1Real, num);

   Result.ResString:= conv.MatrixToStr( mResReal );
end;

Procedure ExprPowerMatrix( var Result: TFPExpressionResult; Const Args: TExprParameterArray);
var
  m1Str : String;
  m1Real, mResReal: t_matrix;
  num : Integer;
  conv : TConversion;
  matrix : TMatrix;
begin
   conv := TConversion.Create;
   matrix:= TMatrix.create();

   num := Args[ 0 ].ResInteger ;
   m1Str := Args[ 1 ].ResString ;

   m1Real := conv.StrToMatrix( m1Str );

   mResReal := matrix.powerMatrix(m1Real, num);

   Result.ResString:= conv.MatrixToStr( mResReal );
end;

Procedure TParseMath.AddFunctions();
begin
   with FParser.Identifiers do begin
       AddFunction('tan', 'F', 'F', @ExprTan);
       AddFunction('sin', 'F', 'F', @ExprSin);
       AddFunction('sen', 'F', 'F', @ExprSin);
       AddFunction('cos', 'F', 'F', @ExprCos);
       AddFunction('ln', 'F', 'F', @ExprLn);
       AddFunction('log', 'F', 'F', @ExprLog);
       AddFunction('sqrt', 'F', 'F', @ExprSQRT);
       AddFunction('floor', 'F', 'F', @ExprFloor );
       AddFunction('power', 'F', 'FF', @ExprPower); //two float arguments 'FF' , returns float
       AddFunction('sinh', 'F', 'F', @ExprSinh );
       AddFunction('cosh', 'F', 'F', @ExprCosh );
       AddFunction('tanh', 'F', 'F', @ExprTanh );

       //AddFunction('Newton', 'F', 'SF', @ExprNewton ); // Una sring argunmen and one float argument, returns float
       {matrix operations}
       AddFunction('msum', 'S', 'SS', @ExprSumMatrix);
       AddFunction('msub', 'S', 'SS', @ExprSubtractMatrix);
       AddFunction('mmult', 'S', 'SS', @ExprMultiplyMatrix);
       AddFunction('mtrans', 'S', 'S', @ExprTransposedMatrix);
       AddFunction('minv', 'S', 'S', @ExprMultiplyMatrix);
       AddFunction('mdet', 'S', 'S', @ExprMultiplyMatrix); // result is a float but is casted to string
       AddFunction('mmultn', 'S', 'SF', @ExprMultiplyByNumberMatrix);
       AddFunction('mpower', 'S', 'SI', @ExprPowerMatrix);

   end

end;

procedure TParseMath.AddVariable( Variable: string; Value: Double );
var Len: Integer;
begin
   Len:= length( identifier ) + 1;
   SetLength( identifier, Len ) ;
   identifier[ Len - 1 ]:= FParser.Identifiers.AddFloatVariable( Variable, Value);

end;

procedure TParseMath.AddString( Variable: string; Value: string );
var Len: Integer;
begin
   Len:= length( identifier ) + 1;
   SetLength( identifier, Len ) ;

   identifier[ Len - 1 ]:= FParser.Identifiers.AddStringVariable( Variable, Value);
end;

end.

