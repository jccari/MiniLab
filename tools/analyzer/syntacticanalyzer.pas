unit syntacticAnalyzer;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils , DataTypes;

// Esta clase estará encargada de parsear los comandos
//  que se pidan realizar en CommandLine

type
  TSAnalyzer = class
    private
      argsList : TStringList;
    public
      constructor Create;
      destructor Destroy;
      function parse(inputCmd: String): TResultAnalyzer;

      function recognizeOperation( inputCmd: String): Integer;
      function extractFunctionArgs( inputCmd: String): TStringList;
      procedure Split(Delimiter: Char; Str: string; ListOfStrings: TStrings) ;
  end;

implementation

constructor TSAnalyzer.Create;
begin
  argsList := TStringList.Create;
end;

destructor TSAnalyzer.Destroy;
begin
  argsList.Destroy;
end;

function TSAnalyzer.parse(inputCmd: String): TResultAnalyzer;
var
  op : Integer;
  nameVar, nameOp, arg : String;
begin
  Result := TResultAnalyzer.Create;
  inputCmd:= Trim(inputCmd);
  inputCmd:= StringReplace( inputCmd, ' ', '', [rfReplaceAll]);

  argsList.Clear;

  try
    op := recognizeOperation(inputCmd);
  except
    op := OPERATION_UNKNOWN;
  end;

  // we need to find exceptions on inputCmd String,
  // example :
     //      Plot('x+1',23,5    // forget a parenthesis
     //      others ...

  case op of
       OPERATION_UNKNOWN:
         Exit;

       NEW_VARIABLE,
       NEW_MATRIX: begin
         nameVar := Copy(inputCmd, 0 , Pos('=',inputCmd)-1 );
         arg := Copy(inputCmd, Pos('=',inputCmd)+1 , inputCmd.Length - 1 );
         argsList.Add( arg );

         Result.variableName:= nameVar;
       end;

       OPERATION_PLOT,
       OPERATION_INTERSECTION,
       OPERATION_INTEGRATE,
       OPERATION_AREA,
       OPERATION_FINDALLROOTS,
       OPERATION_FINDROOT,
       OPERATION_EDO,
       OPERATION_MATRIX,
       OPERATION_EVALUATE_XVALUE,
       OPERATION_INTERPOLATION:
       begin
         nameOp := Copy(inputCmd,0, Pos('(',inputCmd)-1);
         argsList := extractFunctionArgs(inputCmd);

         Result.functionName:= nameOp;
       end;

       //OPERATION_EXTRAPOLATION,
       {
       OPERATION_INTERPOLATION:
       begin
         nameOp := Copy(inputCmd,0, Pos('(',inputCmd)-1);
         argsList := extractFunctionArgs(inputCmd);

         argsList[0] := StringReplace( argsList[0], '[', '', [rfReplaceAll]);
         argsList[0] := StringReplace( argsList[0], ']', '', [rfReplaceAll]);
         argsList[1] := StringReplace( argsList[1], '[', '', [rfReplaceAll]);
         argsList[1] := StringReplace( argsList[1], ']', '', [rfReplaceAll]);

         Result.functionName:= nameOp;
       end;
       }
       OPERATION_EXTRAPOLATION: begin
         nameOp := Copy(inputCmd,0, Pos('(',inputCmd)-1);
         argsList := extractFunctionArgs(inputCmd);

         Result.functionName:= nameOp;
       end;

  end;

  Result.operation := op;
  Result.args := argsList;
  Result.numArgs := Result.args.Count ;
end;

function TSAnalyzer.recognizeOperation( inputCmd: String): Integer;
begin
  inputCmd:= LowerCase( inputCmd );
  if ( Pos('=',inputCmd) > 0 ) then begin
     if ( Pos('=',inputCmd) < Pos('[',inputCmd) ) and ( Pos('=',inputCmd) < Pos(']',inputCmd) ) then begin
        Result:= NEW_MATRIX;
        Exit;
     end;

     Result := NEW_VARIABLE;
     Exit;
  end;
  if ( Pos('plot',inputCmd) > 0 ) then begin
     Result := OPERATION_PLOT;
     Exit;
  end;
  if ( Pos('intersection',inputCmd) > 0 ) then begin
     Result := OPERATION_INTERSECTION;
     Exit;
  end;
  if ( Pos('integrate',inputCmd) > 0 ) then begin
     Result := OPERATION_INTEGRATE;
     Exit;
  end;
  if ( Pos('area',inputCmd) > 0 ) then begin
     Result := OPERATION_AREA;
     Exit;
  end;
  if ( Pos('allroots',inputCmd) > 0 ) then begin
     Result := OPERATION_FINDALLROOTS;
     Exit;
  end;
  if ( Pos('root',inputCmd) > 0 ) then begin
     Result := OPERATION_FINDROOT;
     Exit;
  end;

  if ( Pos('interpolation',inputCmd) > 0 ) then begin
     Result := OPERATION_INTERPOLATION;
     Exit;
  end;
  if ( Pos('extrapolation',inputCmd) > 0 ) then begin
     Result := OPERATION_EXTRAPOLATION;
     Exit;
  end;
  if ( Pos('edo',inputCmd) > 0 ) then begin
     Result := OPERATION_EDO;
     Exit;
  end;
  if ( Pos('evaluate',inputCmd) > 0 ) then begin
     Result := OPERATION_EVALUATE_XVALUE;
     Exit;
  end;
  if ( Pos('msum',inputCmd) > 0 ) or ( Pos('msub',inputCmd) > 0 ) or ( Pos('mmult',inputCmd) > 0 ) then begin
     Result := OPERATION_MATRIX;
     Exit;
  end;

  if ( Pos('clear',inputCmd) > 0 ) then begin
     Result := CLEAR;
     Exit;
  end;

  // last opcion is default
  Result := OPERATION_MATH;

end;

function TSAnalyzer.extractFunctionArgs( inputCmd: String): TStringList;
var
  indexFirst, indexLast : Integer;
  allArgs : String;
begin
  Result := TStringList.Create;

  indexFirst := Pos('(',inputCmd)+1;
  //indexLast := Pos(')',inputCmd);
  indexLast := LastDelimiter(')',inputCmd);

  allArgs:= Copy(inputCmd, indexFirst, indexLast-indexFirst);

  Split(';',allArgs,Result);
end;

procedure TSAnalyzer.Split(Delimiter: Char; Str: string; ListOfStrings: TStrings) ;
begin
   ListOfStrings.Clear;
   ListOfStrings.Delimiter       := Delimiter;
   ListOfStrings.StrictDelimiter := True;
   ListOfStrings.DelimitedText   := Str;
end;

end.

