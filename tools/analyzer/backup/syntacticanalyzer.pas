unit syntacticAnalyzer;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, DataTypes;

type
  TSAnalyzer = class
    private

    public
      constructor Create;
      function analyze(strLine: String): TResultAnalyzer;
  end;

implementation

constructor TSAnalyzer.Create;
begin

end;

function TSAnalyzer.analyze(strLine: String): TResultAnalyzer;
begin
  Result := TResultAnalyzer.Create;
end;

end.

