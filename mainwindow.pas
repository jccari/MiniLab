unit mainwindow;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, uCmdBox, SpkToolbar, spkt_Tab, spkt_Pane,
  spkt_Buttons, Forms, Controls, Graphics, Dialogs, ExtCtrls, ComCtrls, ValEdit,
  StdCtrls, Grids, syntacticAnalyzer, DataTypes, framePlot, ParseMath,
  FunctionOperations, ClosedMethods, mOpenMethod, interpolationMethods,
  TrapezoidalMethod, Simpson1_3Method, Simpson3_8Method, EulerMethod,
  HeunMethod, RungeKMethod, DormandPMethod, ExtrapolationMethods;

type

  { TForm1 }
  TForm1 = class(TForm)
    CommandLine: TCmdBox;
    LblVariableHead: TLabel;
    Memo1: TMemo;
    PageControl1: TPageControl;
    PanelLeft: TPanel;
    PanelRight: TPanel;
    PanelMiddle: TPanel;
    PanelGraph: TPanel;
    SpkLargeButton1: TSpkLargeButton;
    SpkPane1: TSpkPane;
    SpkTab1: TSpkTab;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    Splitter4: TSplitter;
    StrGridPoints: TStringGrid;
    TabSheetVariables: TTabSheet;
    TabSheetCommands: TTabSheet;
    ListEditorVariables: TValueListEditor;
    procedure CommandLineInput(ACmdBox: TCmdBox; Input: string);
    procedure FormCreate(Sender: TObject);
  private
    { private declarations }
    framePlot : TFrame;
    pFramePlot: ^TFrame;
    actualFrame : TFrame;
    framePlot2 : TFrame1;
    parseStr : TSAnalyzer;
    parsemath : TParseMath;
    { ANS atributes }
    ansFunction : String;
    ansMath : Double;
    { operations properties }
    //intersectionFun : TFunOperations;
    //closedRoot : TClosedMethods;
    //openRoot : TOpenMethod;
    //interpolation : TInterpolation;
    //trapecio: TTrapezoidalMethod;
    //simpson13 : TSimpson1_3Method;
    //simpson38 : TSimpson3_8Method;
    {
    euler : TEulerMethod;
    heun : THeunMethod;
    runge : TRungeKuttaMethod;
    dormand : TDormandPrinceMethod;
    }
    //extrapolation : TExtrapolation;
    procedure clearExecute();
    procedure plotExecute(args: TStringList);
    procedure intersectionExecute( args :TStringList); // remove duplicate results
    procedure integrateExecute( args :TStringList); //
    procedure areaExecute( args :TStringList);
    procedure findAllRootsExecute( args :TStringList); //
    procedure findRootExecute( args :TStringList); //
    procedure interpolationExecute( args :TStringList);
    procedure edoExecute( args :TStringList);
    procedure extrapolationExecute( args :TStringList); //
    function chooseTheBestExtrapolation(extrapolation: TExtrapolation; xpoints, ypoints :TStringList): TResult; //
    //procedure Execute( args :TStringList);

    procedure plotFunction(funct :String); //
    procedure plotFunction(funct :String; xmin, xmax :Double); //
    procedure updatePlotFunction(tagFunction :Integer; xmin, xmax:Double);
    procedure plotArea(funct :String; xmin, xmax :Double);
    procedure evaluateFunction( args :TStringList); //
    function evaluateListOfXValues( xvalues :TStringList; funct :String): TStringList; //
    procedure plotPoint( xvalue, yvalue: Double); //
    procedure plotListPoints( xvalues, yvalues: TStringList); //
    procedure plotListXPoints( xvalues :TStringList; yvalue:Double); //

    procedure startCommands();  //

    procedure declareVariable( variable, value: String); //
    procedure loadData( grid :TStringGrid; filename :String); //
    function evaluar(valorX : Double ; f_x : String ) : Double; //
    function BubbleSort( list: TStringList ): TStringList;
  public
    { public declarations }
  end;

var
  Form1: TForm1;

implementation

//uses  framePlot;

const
       COL_NAMEVAR = 0;
       COL_VALUEVAR = 1;
       COL_TIPEVAR = 2;
       COL_XVALUE = 0;
       COL_YVALUE = 1;

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin
  parseStr := TSAnalyzer.Create;
  parsemath := TParseMath.create();
  parsemath.Expression:= '0';
  parsemath.AddVariable('x',0);
  parsemath.Evaluate();

  CommandLine.StartRead(clWhite,clBlack,'>> ',clLime,clBlack);
  CommandLine.TextColor(clLime);

  with ListEditorVariables do begin
    //ColCount:= 3;
    Cells[0,0] := 'Nombre';
    Cells[1,0] := 'Valor';
    //Cells[2,0] := 'Tipo';
    //RowCount:=1;
  end;

  framePlot := TFrame1.Create(PanelGraph);
  framePlot.Parent := PanelGraph;
  framePlot.Align:= alClient;

end;

procedure TForm1.CommandLineInput(ACmdBox: TCmdBox; Input: string);
var
  Res : TResultAnalyzer;
  ResMethods : TResult;
  ResString: String;
  xlist, ylist : TStringList;
begin
  //clearExecute();
  Res := parseStr.parse( Input );
  ResMethods := TResult.Create;
  {
  CommandLine.Writeln( 'Operation : ' + IntToStr(Res.operation) );
  CommandLine.Writeln( 'NVars : ' + Res.variableName );
  CommandLine.Writeln( 'NFunc : ' + Res.functionName );
  CommandLine.Writeln( 'Args : ' + Res.args.CommaText );
  }
  // TODO: agregar excepcion BAD_ARGS para todas las operaciones
  case Res.operation of
       CLEAR: begin
           clearExecute();
           // limpiar variables y graficadora
       end;
       BAD_ARGUMENTS: begin
           CommandLine.Writeln( ERROR_BAD_ARGS );
       end;
       NEW_VARIABLE: begin
           if ( ARGS_NEW_VARIABLE <> Res.numArgs ) then begin
             CommandLine.Writeln( ERROR_DECLARE_VAR );
           end;

           try
             parsemath.AddVariable( Res.variableName, StrToFloat( Res.args[0] ) );
             declareVariable( Res.variableName, Res.args[0] );
           except
             CommandLine.Writeln( ERROR_DECLARE_VAR );
             startCommands();
           end;
       end;

       NEW_MATRIX: begin
           try
             parsemath.AddString( Res.variableName, Res.args[0] );
             declareVariable( Res.variableName, Res.args[0] );
           except
             CommandLine.Writeln( ERROR_DECLARE_VAR );
             startCommands();
           end;
       end;

       OPERATION_PLOT: begin
           try begin
               plotExecute( Res.args );
             //plotFunction( Res.args[0] );
           end
           except
             CommandLine.Writeln( ERROR_PLOT );
             startCommands();
           end;

       end;
       OPERATION_EVALUATE_XVALUE: begin
         try
           evaluateFunction( Res.args );
         except
           CommandLine.Writeln( ERROR_EVALUATE_XVALUE );
         end;
       end;

       OPERATION_INTERSECTION: begin // falta eliminar las respuestas repetidas
           try
             intersectionExecute( Res.args );
           except
             CommandLine.Writeln( ERROR_INTERSECTION );
             startCommands();
           end;
       end;

       OPERATION_INTEGRATE: begin
         try
           integrateExecute( Res.args );
         except
           CommandLine.Writeln( ERROR_INTEGRATE );
         end;
       end;

       OPERATION_AREA: begin
         try
         areaExecute( Res.args );
         except
           CommandLine.Writeln( ERROR_AREA );
         end;
       end;

       OPERATION_FINDALLROOTS: begin
         try
           findAllRootsExecute( Res.args );
         except
           CommandLine.Writeln( ERROR_FINDALLROOTS );
         end;

       end;

       OPERATION_FINDROOT: begin // falta plotear las funciones ingresadas y respuestas
         try
          findRootExecute( Res.args );
         except
           CommandLine.Writeln( ERROR_FINDROOT );
         end;
       end;

       OPERATION_INTERPOLATION: begin
         // falta hallar el comportamiento de la funcion respuesta, es decir plotear
         // falta poder evaluar un X en la funcion resultante
         interpolationExecute(Res.args);
       end;

       OPERATION_EXTRAPOLATION: begin
           try
             extrapolationExecute( Res.args );
           except
             CommandLine.Writeln( ERROR_EXTRAPOLATION );
           end;
       end;

       OPERATION_EDO: begin  // falta hallar el comportamiento de la funcion respuesta, es decir plotear
         try
          edoExecute( Res.args );
         except
           CommandLine.Writeln( ERROR_EDO );
         end;
       end;

       OPERATION_MATRIX : begin
         parsemath.Expression:= Input;
         CommandLine.Writeln( parsemath.EvaluateStr() );
       end;

       OPERATION_MATH: begin
         parsemath.Expression:= Input;
         CommandLine.Writeln( FloatToStr( parsemath.Evaluate() ) );
       end;

  end;
  startCommands();
end;

// CALL -> intersection( fun1, fun2 )
// CALL -> intersection( fun1, fun2 , xmin, xmax) // do it XD
procedure TForm1.intersectionExecute( args :TStringList);
var
  intersectionFun :TFunOperations;
  Res : TResult;
  xValues, yValues: TStringList;
  funct1, funct2: String;
  xmin, xmax :Double;
begin
  intersectionFun := TFunOperations.Create;
  xValues := TStringList.Create;
  yValues := TStringList.Create;
  funct1:= args[0];
  funct2:= args[1];

  case args.Count of
       2: begin
           Res := intersectionFun.intersection( funct1, funct2, DEFAULT_XMIN, DEFAULT_XMAX, DEFAULT_ERROR, 1.0);
           // remove duplicate results
           if ( Res.matrix.IntersectionList.Count > 0 ) then begin
             xValues := Res.matrix.IntersectionList;
             CommandLine.Writeln( 'Result: ' );
             CommandLine.Writeln( xValues.CommaText );
           end
           else
             CommandLine.Writeln( 'There are no intersections' );

           yValues := evaluateListOfXValues( xValues, funct1 );

           plotFunction( funct1, DEFAULT_XMIN, DEFAULT_XMAX);
           plotFunction( funct2, DEFAULT_XMIN, DEFAULT_XMAX);
           plotListPoints( xValues, yValues);
       end;
       4: begin
           xmin := StrToFloat( args[2] );
           xmax := StrToFloat( args[3] );

           Res := intersectionFun.intersection( funct1, funct2, xmin , xmax, DEFAULT_ERROR, 1.0);
           // remove duplicate results
           if ( Res.matrix.IntersectionList.Count > 0 ) then begin
             xValues := Res.matrix.IntersectionList;
             CommandLine.Writeln( 'Result: ' );
             CommandLine.Writeln( xValues.CommaText );
           end
           else
             CommandLine.Writeln( 'There are no intersections' );

           yValues := evaluateListOfXValues( xValues, funct1 );

           plotFunction( funct1, xmin-1, xmax+1);
           plotFunction( funct2, xmin-1, xmax+1);
           plotListPoints( xValues, yValues);
       end;
  else
    CommandLine.Writeln( ERROR_BAD_ARGS );
  end;
  StrGridPoints.RowCount := xValues.Count ;
  StrGridPoints.Cols[ COL_XVALUE ].CommaText:= xValues.CommaText;
  StrGridPoints.Cols[ COL_YVALUE ].CommaText:= yValues.CommaText;
end;

// CALL -> integrate( function; xmin; xmax)
// CALL -> integrate( function; xmin; xmax; method )
procedure TForm1.integrateExecute( args :TStringList);
var
  Res :TResult;
  trapecio :TTrapezoidalMethod;
  simpson13 :TSimpson1_3Method;
  simpson38 :TSimpson3_8Method;
  xmin, xmax :Double;
  funct : String;
begin
  funct:= args[0];
  xmin:= StrToFloat( args[1]);
  xmax:= StrToFloat( args[2]);

  case args.Count of
       3: begin
           simpson38 := TSimpson3_8Method.create();
           simpson38.setParameters(args[0], xmin, xmax, 150);
           Res := simpson38.eval();

           CommandLine.Writeln('Result: '+ Res.result );
           Memo1.Lines.Add( Res.result );
           simpson38.destroy();
       end;

       4: begin
           case LowerCase( args[3] ) of
                'trapecio': begin
                  trapecio := TTrapezoidalMethod.create();
                  trapecio.setParameters( args[0], StrToFloat( args[1]), StrToFloat( args[2]), 4);
                  Res := trapecio.eval();

                  CommandLine.Writeln('Result: '+ Res.result );
                  Memo1.Lines.Add( Res.result );
                  trapecio.destroy();
                  end;
                'simpson13': begin
                  simpson13 := TSimpson1_3Method.create();
                  simpson13.setParameters( args[0],StrToFloat( args[1]), StrToFloat( args[2]), 4);
                  Res := simpson13.eval();

                  CommandLine.Writeln('Result: '+ Res.result );
                  Memo1.Lines.Add( Res.result );
                  simpson13.destroy();
                  end;
                'simpson38': begin
                  simpson38 := TSimpson3_8Method.create();
                  simpson38.setParameters(args[0],StrToFloat( args[1]), StrToFloat( args[2]), 150);
                  Res := simpson38.eval();

                  CommandLine.Writeln('Result: '+ Res.result );
                  Memo1.Lines.Add( Res.result );
                  simpson38.destroy();
                  end;
           end;
       end

  else begin
      CommandLine.Writeln( ERROR_BAD_ARGS );
      startCommands();
    end;
  end;
  plotFunction(funct, xmin-1, xmax+1);
  plotArea( funct, xmin, xmax );
end;

// area( funct1, funct2)
// area( funct1, funct2, xmin, xmax)
procedure TForm1.areaExecute( args :TStringList);
var
  Res, ResIntersection :TResult;
  intersection: TFunOperations;
  xmin, xmax :Double;
  funct1, funct2 : String;
  xvalues, yvalues: TStringList;
  areaTotal, areaParcial: Double;

  function calculateArea( funct:String; xini, xlast: Double): Double;
  var simpson38 :TSimpson3_8Method;
  begin
    simpson38 := TSimpson3_8Method.create();
    simpson38.setParameters( funct, xini, xlast, 180);
    Result := StrToFloat( simpson38.eval().result );
  end;

  procedure calculateArea(_funct1, _funct2:String; xini, xlast: Double);
  var i : Integer;
      tmp1, tmp2 :Double;
      functAbs: String;
  begin
    xvalues := TStringList.Create;
    yvalues := TStringList.Create;
    intersection := TFunOperations.create();
    xvalues := intersection.intersection( _funct1, _funct2, xini, xlast, DEFAULT_ERROR, 1.0).matrix.IntersectionList;
    //xvalues.Add( FloatToStr(xini) );
    //xvalues.Add( FloatToStr(xlast) );
    xvalues := BubbleSort( xvalues );
    //yvalues := evaluateListOfXValues( xvalues, funct1);

    plotFunction(_funct1, xini, xlast);
    plotFunction(_funct2, xini, xlast);
    //plotArea(_funct1, xini, xlast);

    functAbs:= 'abs(' +funct1+'-('+ funct2+') )';
    areaTotal:= 0;
    for i:=0 to xvalues.Count-2 do begin
      xini:= StrToFloat( xvalues[i] );
      xlast:= StrToFloat( xvalues[i+1] );

      tmp1:= calculateArea( functAbs , xini, xlast);
      //tmp1:= calculateArea( 'abs('+ _funct1+')', xini, xlast);
      //tmp2:= calculateArea( 'abs('+ _funct2+')', xini, xlast);

      areaParcial:= abs( tmp1 ) ; //- abs(tmp2) ;
      //CommandLine.Writeln( 'Area1: '+ FloatToStr( areaParcial ));
      //CommandLine.Writeln( 'Area2: '+ FloatToStr( calculateArea( _funct2, xini, xlast) ));
      areaTotal:= areaTotal + areaParcial ;

      plotArea(_funct1, xini, xlast);
    end;
    CommandLine.Writeln( 'Result: '+ FloatToStr( areaTotal ));
    //xvalues.Delete( xvalues.Count-1);
    //xvalues.Delete(0);
    yvalues := evaluateListOfXValues( xvalues, funct1);

    StrGridPoints.RowCount:= xvalues.Count;
    StrGridPoints.Cols[ COL_XVALUE ].CommaText := xvalues.CommaText;
    StrGridPoints.Cols[ COL_YVALUE ].CommaText := yvalues.CommaText;

    plotListPoints(xvalues, yvalues);
  end;

begin
  funct1:= args[0] ;
  funct2:= args[1] ;
  case args.Count of
       2: begin
         // hallar puntos de interseccion
         // hallar area cada 2 puntos
         calculateArea( funct1, funct2, DEFAULT_XMIN, DEFAULT_XMAX);
       end;
       4: begin
         xmin:= StrToFloat( args[2]);
         xmax:= StrToFloat( args[3]);

         calculateArea( funct1, funct2, xmin, xmax);
       end;
  end;

end;

procedure TForm1.findAllRootsExecute( args :TStringList);
var
  intersectionFun :TFunOperations;
  Res : TResult;
  funct1, funct2: String;
  XValues: TStringList;
  YValues: TStringList;
begin
  intersectionFun := TFunOperations.Create;
  XValues := TStringList.Create;
  YValues := TStringList.Create;
  funct1:= args[0];
  funct2:= '0';

  Res := intersectionFun.intersection( funct1, funct2, DEFAULT_XMIN, DEFAULT_XMAX, DEFAULT_ERROR, 1.0);
  // remove duplicate results
  if ( Res.matrix.IntersectionList.Count > 0 ) then begin
    CommandLine.Writeln( 'Result: ' );
    XValues := Res.matrix.IntersectionList;
    CommandLine.Writeln( XValues.CommaText );
  end
  else
    CommandLine.Writeln( 'There are no roots' );

  plotFunction(funct1);
  YValues := evaluateListOfXValues( XValues, funct1);

  StrGridPoints.Cols[COL_XVALUE].CommaText:= XValues.CommaText;
  StrGridPoints.Cols[COL_YVALUE].CommaText:= XValues.CommaText;

  plotListPoints(XValues,YValues);
end;

// root( func; a; b; biseccion)
// root( func; a; b; falsaposicion)
// root( func; funcDerivada; a; newton)
// root( func; a; secante)
procedure TForm1.findRootExecute( args :TStringList);
var
  closedRoot: TClosedMethods;
  openRoot: TOpenMethod;
  funct, funcDeriv : String;
  a, b, xresult :Double;
  method: String;
  Res: TResult;

  procedure plotPointStr(xvalueStr , functExpr: String);
  var xvalue:Double;
  begin
    try
       xvalue:= StrToFloat(xvalueStr);
       plotPoint(xvalue, evaluar( xvalue, functExpr));
    except
      CommandLine.Writeln( 'Can not plot point' );
    end;
  end;

begin
  Res := TResult.Create;
  closedRoot := TClosedMethods.create();
  openRoot := TOpenMethod.Create;
  funct:= args[0];
  case args.Count of
       3: begin
           a := StrToFloat(args[1]);
           method:= args[2];
           if ( method = 'secante') then begin
             Res := openRoot.secante( a, funct, DEFAULT_ERROR );
             CommandLine.Writeln( 'Result: '+ Res.result );
           end;

           plotFunction( funct, a-2, a+2);
           plotPointStr( Res.result, funct);
       end;
       4: begin
           case LowerCase(args[3]) of
                'biseccion': begin
                  a := StrToFloat(args[1]);
                  b := StrToFloat(args[2]);

                  Res := closedRoot.bisectionMethod( a, b, DEFAULT_ERROR, funct );
                  CommandLine.Writeln( 'Result: '+ Res.result );
                  plotFunction( funct, a-2, b+2);
                  plotPointStr( Res.result, funct);
                end;
                'falsaposicion': begin
                  a := StrToFloat(args[1]);
                  b := StrToFloat(args[2]);

                  Res := closedRoot.fakePositionMethod(a, b, DEFAULT_ERROR, funct );
                  CommandLine.Writeln( 'Result: '+ Res.result );
                  plotFunction( funct, a-2, b+2);
                  plotPointStr( Res.result, funct);
                end;
                 // this method need 4
                'newton': begin
                  funcDeriv := args[1];
                  a := StrToFloat(args[2]);

                  Res := openRoot.newton( a, funct, funcDeriv, DEFAULT_ERROR );
                  CommandLine.Writeln('Result: '+ Res.result );
                  plotFunction( funct, a-2, a+2);
                  plotPointStr( Res.result, funct);
                end;
           end;
       end;
  end;

end;

// interpolation(filename)
procedure TForm1.interpolationExecute( args :TStringList);
var
  interpolation: TInterpolation;
  filename :String;
  xlist, ylist: TStringList;
  ResString: string;
begin
  filename:= args[0];

  interpolation := TInterpolation.Create;
  xlist := TStringList.Create;
  ylist := TStringList.Create;

  loadData( StrGridPoints, args[0]);
  //StrGridPoints.LoadFromCSVFile( filename );

  xlist.CommaText:= StrGridPoints.Cols[ COL_XVALUE ].CommaText;
  ylist.CommaText:= StrGridPoints.Cols[ COL_YVALUE ].CommaText;;

  ResString:= interpolation.lagrange(xlist,ylist);
  CommandLine.Writeln( ResString );
  Memo1.Lines.Add( ResString );
  ansFunction := ResString;

  plotFunction(ResString, StrToFloat(xlist[0]) , StrToFloat(xlist[xlist.Count-1]) );
end;

// edo( funct; xini; yini; xlast)
// edo( funct; xini; yini; xlast; method)
procedure TForm1.edoExecute( args :TStringList);
var
  method, funct, functOriginal: String;
  xini, yini, xlast: Double;
  euler: TEulerMethod;
  heun: THeunMethod;
  runge: TRungeKuttaMethod;
  dormand: TDormandPrinceMethod;
  extrapolation: TExtrapolation;
  Res: TResult;
  xnlist, ynlist: TStringList;
  interpolation: TInterpolation;
  n: Integer;
begin
  Res := TResult.Create;
  interpolation := TInterpolation.Create;
  extrapolation := TExtrapolation.Create();
  xnlist := TStringList.Create;
  ynlist := TStringList.Create;

  funct:= args[0];
  xini:= StrToFloat( args[1] );
  yini:= StrToFloat( args[2] );
  xlast:=StrToFloat( args[3] );
  n := StrToInt( args[4] );
  case args.Count of
       5: begin
         runge := TRungeKuttaMethod.create();
         runge.setData( funct, xini, yini, xlast,n ) ;
         Res := runge.solve();
         xnlist := Res.matrix.XnList;
         ynlist := Res.matrix.YnList;
         //CommandLine.Writeln( Res.matrix.XnList.CommaText );
         //CommandLine.Writeln( Res.matrix.YnList.CommaText );
       end;
       6: begin
           method:= args[5];
           case LowerCase( method ) of
                'euler': begin
                  euler := TEulerMethod.create();
                  euler.setData( funct, xini, yini, xlast,n ) ;
                  Res := euler.solve();

                  xnlist := Res.matrix.XnList;
                  ynlist := Res.matrix.YnList;
                  //CommandLine.Writeln( Res.matrix.XnList.CommaText );
                  //CommandLine.Writeln( Res.matrix.YnList.CommaText );
                end;
                'heun': begin
                  heun := THeunMethod.create();
                  heun.setData( funct, xini, yini, xlast,n ) ;
                  Res := heun.solve();

                  xnlist := Res.matrix.XnList;
                  ynlist := Res.matrix.YnList;
                  //CommandLine.Writeln( Res.matrix.XnList.CommaText );
                  //CommandLine.Writeln( Res.matrix.YnList.CommaText );
                end;
                'rungekutta': begin
                  runge := TRungeKuttaMethod.create();
                  runge.setData( funct, xini, yini, xlast,n ) ;
                  Res := runge.solve();

                  xnlist := Res.matrix.XnList;
                  ynlist := Res.matrix.YnList;
                  //CommandLine.Writeln( Res.matrix.XnList.CommaText );
                  //CommandLine.Writeln( Res.matrix.YnList.CommaText );
                end;
                'dormantprince': begin
                  dormand := TDormandPrinceMethod.create();
                  dormand.setData( funct, xini, yini, xlast,n ) ;
                  Res := dormand.solve();

                  xnlist := Res.matrix.XnList;
                  ynlist := Res.matrix.YnList;
                  //CommandLine.Writeln( Res.matrix.XnList.CommaText );
                  //CommandLine.Writeln( Res.matrix.YnList.CommaText );
                end;
           end;
       end;
  end;

  //functOriginal := chooseTheBestExtrapolation( extrapolation, xnlist, ynlist).result ;
  functOriginal:= interpolation.lagrange( xnlist,ynlist);
  CommandLine.Writeln( 'Result: '+ functOriginal );

  plotFunction( functOriginal, xini, xlast );
  plotListPoints( xnlist, ynlist);
  StrGridPoints.RowCount:= xnlist.Count;
  StrGridPoints.Cols[COL_XVALUE].CommaText:= xnlist.CommaText;
  StrGridPoints.Cols[COL_YVALUE].CommaText:= ynlist.CommaText;

  ansFunction:= functOriginal;
end;

// extrapolation( filename; method)
// extrapolation( filename )
procedure TForm1.extrapolationExecute( args :TStringList);
var
  xpoints, ypoints: TStringList;
  Res : TResult;
  extrapolation : TExtrapolation;
begin
  Res := TResult.Create;
  extrapolation := TExtrapolation.Create();

  xpoints := TStringList.Create;
  ypoints := TStringList.Create;

  loadData( StrGridPoints, args[0]);

  xpoints.CommaText := StrGridPoints.Cols[COL_XVALUE].CommaText;
  ypoints.CommaText := StrGridPoints.Cols[COL_YVALUE].CommaText;

  case args.Count of
       1: begin
           Res := chooseTheBestExtrapolation(extrapolation, xpoints, ypoints);
           CommandLine.Writeln( 'Best Extrapolation: ' + Res.result );
           CommandLine.Writeln( 'R coeficient: ' + Res.r );
       end;
       2: begin
           case LowerCase( args[1] ) of
                'linear': begin
                  Res := extrapolation.linear( xpoints, ypoints);
                  CommandLine.Writeln('Result: ' + Res.result );
                  CommandLine.Writeln('R coeficient: ' + Res.r );
                end;
                'exponential': begin
                  Res := extrapolation.exponential( xpoints, ypoints);
                  CommandLine.Writeln('Result: ' + Res.result );
                  CommandLine.Writeln('R coeficient: ' + Res.r );
                end;
                'logarithmic': begin
                  Res := extrapolation.logarithmic( xpoints, ypoints);
                  CommandLine.Writeln('Result: ' + Res.result );
                  CommandLine.Writeln('R coeficient: ' + Res.r );
                end;
           end;
       end;
  else
    CommandLine.Writeln(ERROR_BAD_ARGS);
  end;

  plotFunction(Res.result);
  plotListPoints( xpoints, ypoints);
  self.ansFunction:= Res.result ;

  xpoints.Destroy;
  ypoints.Destroy;
  extrapolation.Destroy;
end;

function TForm1.chooseTheBestExtrapolation(extrapolation: TExtrapolation; xpoints, ypoints :TStringList): TResult;
var
  bestR :Double;
  bestMethod, i :Integer;
  resMethods, resRMethods : TStringList;
begin
  Result := TResult.Create;
  resMethods := TStringList.Create;
  resRMethods := TStringList.Create;
  bestR := 0;

  resMethods.Add( extrapolation.linear( xpoints, ypoints).result );
  resRMethods.Add( extrapolation.linear( xpoints, ypoints).r );

  resMethods.Add( extrapolation.exponential( xpoints, ypoints).result );
  resRMethods.Add( extrapolation.exponential( xpoints, ypoints).r );

  resMethods.Add( extrapolation.logarithmic( xpoints, ypoints).result );
  resRMethods.Add( extrapolation.logarithmic( xpoints, ypoints).r );

  bestMethod:= 0;
  bestR := StrToFloat( resRMethods[0] );
  for i:= 1 to resMethods.Count-1 do begin
    if ( StrToFloat( resRMethods[i] ) > bestR ) then begin
       bestMethod := i;
       bestR:= StrToFloat( resRMethods[i] );
    end;
  end;

  Result.result:= resMethods[bestMethod];
  Result.r:= resRMethods[bestMethod];

  resMethods.Destroy;
  resRMethods.Destroy;
end;

procedure TForm1.plotExecute( args: TStringList);
var
  funct: String;
  xmin, xmax: Double;
begin
  funct:= args[0];
  case args.Count of
       1: begin
         plotFunction( funct );
       end;
       3: begin
         xmin:= StrToFloat( args[1] );
         xmax:= StrToFloat( args[2] );
         plotFunction( funct, xmin, xmax );
       end;
  end;
end;

procedure TForm1.plotFunction(funct :String);
begin
  TFrame1(framePlot).PlotFunction(funct, DEFAULT_XMIN, DEFAULT_XMAX);
end;

procedure TForm1.plotFunction(funct :String; xmin, xmax :Double);
begin
  TFrame1(framePlot).PlotFunction(funct, xmin, xmax);
end;

procedure TForm1.updatePlotFunction(tagFunction :Integer; xmin, xmax:Double);
begin

end;

procedure TForm1.plotArea(funct :String; xmin, xmax :Double);
begin
  TFrame1(framePlot).PlotArea(funct, xmin, xmax);
end;

procedure TForm1.evaluateFunction( args: TStringList);
var
  funct: String;
  value: Double;
begin
  case args.Count of
       1: begin
             value:= StrToFloat( args[0] );
             parsemath.Expression:= ansFunction ;
             parsemath.NewValue('x', value );
             CommandLine.Writeln('Result: '+ FloatToStr( parsemath.Evaluate() ));
             TFrame1(framePlot).PlotPoint( value , parsemath.Evaluate() );
       end;
       2: begin
         funct := args[0];
         value := StrToFloat( args[1] );

         parsemath.Expression:= funct ;
         parsemath.NewValue('x', value );
         CommandLine.Writeln('Result: '+ FloatToStr( parsemath.Evaluate() ));
         TFrame1(framePlot).PlotFunction( funct, value-3, value+3 );
         TFrame1(framePlot).PlotPoint( value , parsemath.Evaluate() );
       end;
  else
    CommandLine.Writeln( ERROR_BAD_ARGS );
  end;
end;

function TForm1.evaluateListOfXValues( xvalues :TStringList; funct: String): TStringList;
var
  yvalues: TStringList;
  i :Integer;
  y : Double;
begin
  yvalues := TStringList.Create;
  for i:=0 to xvalues.Count-1 do begin
    y := evaluar( StrToFloat(xvalues[i]) , funct);
    yvalues.Add( FloatToStr( y ) );
  end;
  Result := yvalues;
end;

procedure TForm1.plotPoint( xvalue, yvalue: Double);
begin
  TFrame1(framePlot).plotPoint( xvalue, yvalue);
end;

procedure TForm1.plotListPoints( xvalues, yvalues: TStringList);
begin
  TFrame1(framePlot).PlotPointList( xvalues, yvalues);
end;

procedure TForm1.plotListXPoints( xvalues :TStringList; yvalue:Double);
var
  yvalues: TStringList;
  i :Integer;
begin
  yvalues := TStringList.Create;
  for i:= 0 to xvalues.Count-1 do
      yvalues.Add( FloatToStr(yvalue) );

  TFrame1(framePlot).PlotPointList( xvalues, yvalues);
end;

procedure TForm1.startCommands();
begin
  CommandLine.StartRead(clWhite,clBlack,'>> ',clLime,clBlack);
end;

procedure TForm1.declareVariable( variable, value: String);
var row : Integer;
begin
  row := ListEditorVariables.RowCount - 1 ;
  ListEditorVariables.Cells[COL_NAMEVAR,row] := variable;
  ListEditorVariables.Cells[COL_VALUEVAR,row] := value;
  //ListEditorVariables.Cells[COL_TIPEVAR,row] := 'var';
  ListEditorVariables.RowCount := ListEditorVariables.RowCount + 1;

  //parsemath.AddVariable( variable, StrToFloat(value) );
end;

procedure TForm1.loadData( grid :TStringGrid; filename :String);
begin
  grid.LoadFromCSVFile(filename);
end;

function TForm1.evaluar(valorX : Double ; f_x : String ) : Double;
var
   MiParse : TParseMath;
begin
  try
   Miparse := TParseMath.create();
   MiParse.AddVariable('x',valorX);
   MiParse.Expression:= f_x;
   evaluar := MiParse.Evaluate();
   MiParse.destroy;
  except
     evaluar:=0.0001;
     Exit;
  end;
end;

function TForm1.BubbleSort( list: TStringList ): TStringList;
var
  i, j: Integer;
  temp: string;
begin
  // bubble sort
  for i := 0 to list.Count - 1 do begin
    for j := 0 to ( list.Count - 1 ) - i do begin
      // Condition to handle i=0 & j = 9. j+1 tries to access x[10] which
      // is not there in zero based array
      if ( j + 1 = list.Count ) then
        continue;
      if ( StrToFloat( list[j]) > StrToFloat( list[j+1]) ) then begin
        temp      := list[j];
        list[j]   := list[j+1];
        list[j+1] := temp;
      end; // endif
    end; // endwhile
  end; // endwhile
  Result := list;
end;

procedure TForm1.clearExecute();
begin
  //TFrame1(framePlot).Chart1.ClearSeries;
  TFrame1(framePlot).clear();
  CommandLine.Clear;
  StrGridPoints.Clear;
end;

end.

