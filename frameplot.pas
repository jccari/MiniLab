unit framePlot;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, TAGraph, TASeries, Forms, math, Controls, ParseMath, DataTypes;

type

  { TFrame1 }

  TFrame1 = class(TFrame)
    Chart1: TChart;
    FunctionSerie: TLineSeries;
    AreaSerie: TAreaSeries;
    PointsSerie: TLineSeries;
  private
    { private declarations }
    FunctionList : TList;
    ActiveFunction : Integer;
    min,
    max : Real;
    Parse : TParseMath;

    procedure CreateNewFunction( expressionFunc: String);
    procedure Graphic2D( xmin, xmax: Double);
    procedure DrawLineSerie( functSerie: TLineSeries; funExpression:String; xmin,xmax: Double);
    procedure DrawPoint(x,y: Double; functSerie: TLineSeries);
    procedure DrawPointList(xPoints,yPoints: TStringList; functSerie: TLineSeries );

    function f( x: Real ): Real;
    function evaluar( x: Real; fun: String ): Real;

  public
    { public declarations }
    testText : String;
    PointsList: TStringList;
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy(); override;
    procedure PlotFunction( expressionFunc: String; xmin, xmax: Double);
    procedure PlotFunction( expressionFunc: String );
    procedure PlotArea( expressionFunc: String; xmin, xmax: Double);
    procedure PlotPoint( xvalue, yvalue: Double);
    procedure PlotPointList( xPoints, yPoints: TStringList);
    procedure clear();
  end;

implementation

const
     FunctionSeriesName = 'FunctionLines';
     //h = 0.01;

{$R *.lfm}

constructor TFrame1.Create(TheOwner: TComponent);
begin
  Inherited Create(TheOwner);
  FunctionList := TList.Create;
  Parse := TParseMath.create();
  Parse.AddVariable('x',0);
  PointsList := TStringList.Create;
  testText:= 'Frame is created!';
end;

destructor TFrame1.Destroy();
var i: Integer;
begin
  inherited Destroy;
  for i:=0 to FunctionList.Count-1 do begin
      TLineSeries( FunctionList[i] ).Destroy;
  end;
  FunctionList.Destroy;
  Parse.destroy;
end;

procedure TFrame1.clear();
var i:Integer;
begin
  for i:=0 to FunctionList.Count-1 do begin
      TLineSeries(FunctionList[i]).Clear;
  end;
  FunctionSerie.Clear;
  //PointsList.Clear;
  AreaSerie.Clear;
  PointsSerie.Clear;
end;

procedure TFrame1.PlotFunction( expressionFunc: String; xmin, xmax: Double);
var x :Double;
const h = 0.01;
begin
  //Chart1.ClearSeries;

  CreateNewFunction( expressionFunc );
  DrawLineSerie( TLineSeries( FunctionList[ ActiveFunction ] ) , expressionFunc, xmin, xmax );

  testText:= TLineSeries( FunctionList[ ActiveFunction ]).Name;
  //testText:= 'otra cosa';
end;

procedure TFrame1.PlotFunction( expressionFunc: String );
var x :Double;
const h = 0.01;
begin
  //Chart1.ClearSeries;

  CreateNewFunction( expressionFunc );
  DrawLineSerie( TLineSeries( FunctionList[ ActiveFunction ] ) , expressionFunc, DEFAULT_XMIN, DEFAULT_XMIN );

  testText:= TLineSeries( FunctionList[ ActiveFunction ]).Name;
end;

procedure TFrame1.PlotArea( expressionFunc: String; xmin, xmax: Double);
var
  x:Double;
const
  h = 0.01;
begin
  x:= xmin;
  //AreaSerie.Clear;
  //AreaSerie.;
  with AreaSerie do
  repeat
        AddXY( x, evaluar(x, expressionFunc) );
        x := x + h ;
  until ( x >= xmax );
end;


procedure TFrame1.PlotPoint( xvalue, yvalue: Double);
begin
  DrawPoint( xvalue, yvalue, PointsSerie );
  DrawPoint( NaN, NaN, PointsSerie );
end;

procedure TFrame1.PlotPointList( xPoints, yPoints: TStringList);
begin
  DrawPointList( xPoints, yPoints, PointsSerie );
end;

procedure TFrame1.CreateNewFunction( expressionFunc: String);
begin
   //We create serial functions
  FunctionList.Add( TLineSeries.Create( Chart1 ) );
  with TLineSeries( FunctionList[ FunctionList.Count - 1 ] ) do begin
      Title:= expressionFunc ;
      Name:= FunctionSeriesName + IntToStr( FunctionList.Count );
      Tag:= FunctionList.Count - 1;
      LinePen.Width:= 2 ;
  end;
  ActiveFunction:= FunctionList.Count - 1;
  Chart1.AddSeries( TLineSeries( FunctionList[ FunctionList.Count - 1 ] ) );

end;

procedure TFrame1.Graphic2D( xmin, xmax: Double);
var x: Real;
const h = 0.01;
begin
  x:= xmin;
  TLineSeries( FunctionList[ ActiveFunction ] ).Clear;
  with TLineSeries( FunctionList[ ActiveFunction ] ) do
  repeat
      AddXY( x, f(x) );
      x:= x + h ;
  until ( x>= xmax );

end;

procedure TFrame1.DrawLineSerie( functSerie: TLineSeries; funExpression:String ;xmin,xmax: Double);
var x: Double;
const h = 0.01;
begin
  x:= xmin;
  functSerie.Clear;

  with TLineSeries(functSerie) do
  repeat
      AddXY( x, evaluar(x,funExpression) );
      x := x + h ;
  until ( x >= xmax );

end;

procedure TFrame1.DrawPoint(x,y: Double; functSerie: TLineSeries);
begin
    functSerie.AddXY(x,y);
end;

procedure TFrame1.DrawPointList(xPoints,yPoints: TStringList; functSerie: TLineSeries );
var i : Integer;
begin
    for i:=0 to xPoints.Count-1 do begin
        DrawPoint( StrToFloat( xPoints[i] ), StrToFloat( yPoints[i] ), functSerie);
        DrawPoint(NaN,NaN,functSerie);
    end;
end;

function TFrame1.f( x: Real ): Real;
begin
  Parse.Expression:= TLineSeries(FunctionList[ ActiveFunction ]).Title;
  Parse.NewValue('x', x);
  Result:= Parse.Evaluate();
end;

function TFrame1.evaluar( x: Real; fun: String ): Real;
begin
  Parse.Expression:= fun;
  Parse.NewValue('x', x);
  Result:= Parse.Evaluate();
end;

end.

